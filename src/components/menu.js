import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import Layout from "../layout/layoutDeTelaEstrutura";
import LayoutHorizontal from "../layout/layoutHorizontal";
import LayoutDeGrade from "../layout/layoutDeGrade";
import Components from "./components";

export default function Menu({navigation}) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("TelaInicial")}
      >
        <Text>opção 1</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("SegundaPagina")}
      >
        <Text>opção 2</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}>
        <Text>opção 3</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  menu: {
    padding: 10,
    margin: 5,
    backgroundColor: "#FFC0CB",
    borderRadius: 5,
  },
});
