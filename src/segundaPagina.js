import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Button,
} from "react-native";

function SegundaPagina({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>SEGUNDA PÁGINA</Text>
      <Button
        title="Voltar para tela inicial"
        onPress={() => navigation.goBack()}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default SegundaPagina;
