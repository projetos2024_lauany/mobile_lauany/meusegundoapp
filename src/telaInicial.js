import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Button,
} from "react-native";

function TelaInicial({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>TELA INICIAL</Text>
      <Button
        title="Ir para segunda página"
        onPress={() => navigation.navigate("SegundaPagina")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
  });
  
export default TelaInicial;

