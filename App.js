import SegundaPagina from "./src/segundaPagina";
import TelaInicial from "./src/telaInicial";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import Menu from "./src/components/menu";

const Stack = createStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="Menu" component={Menu} />
        <Stack.Screen name="TelaInicial" component={TelaInicial} />
        <Stack.Screen name="SegundaPagina" component={SegundaPagina} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}